// const express = require("express");
import express from 'express';

// const http = require("http");
import http from 'http';

import { fileURLToPath } from 'url';
import json from 'body-parser';
import path from 'path';

const port = 3000; // AWS port
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
import misRutas from './router/index.js';

// Declarar la variable punto de inicio

const main = express();
export const router = express.Router();

main.set("view engine", "ejs"); // Motor de renderizado 
main.set(express.static(__dirname + '/public'));
main.use(json.urlencoded({extended:true}));
main.use(misRutas.router)

main.listen(port, () => {
    console.log(`Se inició el servidor por el puerto ${port}`)
});
