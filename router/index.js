import express from 'express';
import json from 'body-parser';

export const router = express.Router();
export default {router};

// Se declara primera ruta por omisión
router.get('/', (req, res) => {
    res.render('index', {titulo: "Practicas NodeJS", nombre: "Jonathan Alexis"});
});

router.get('/table', (req, res) => {
    // Parámetros
    const params = {
        numero: req.query.numero,
    }
    res.render('table', params);
});

router.post('/table', (req, res) => {
    // Parámetros
    const params = {
        numero: req.body.numero,
    }
    res.render('table', params);
});

